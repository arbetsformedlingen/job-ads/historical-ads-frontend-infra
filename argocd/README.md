# ArgoCD

Aardvark is deployed using ArgoCD at JobTech. File for deployment can be
found in https://gitlab.com/arbetsformedlingen/devops/argocd-infra.

File argocd-deployer-access-to-project.yaml sets the permissions for
ArgoCD to deploy.
